# Analyzing RNA-Seq data with Galaxy

This is the material for the galaxy workshop on RNA-seq data analysis at EMBL Rome. This is the first time that this workshop is proposed. We will start with a 2 days time frame and will adjust.

All materials used in this workshop are on the [Galaxy training website](https://galaxyproject.github.io/training-material/).

## What is Galaxy?

**Slides**: [Introduction to Galaxy](https://galaxyproject.github.io/training-material/topics/introduction/slides/introduction.html#1).

## Galaxy at EMBL

EMBL has its own instance of Galaxy at [https://galaxy.embl.de/](https://galaxy.embl.de/).

=> Demo of data import at EMBL.

**Warning**: There will be slight differences with the official tutorials. **Always check the instructions in the 'Differences with the tutorial' section below the tutorial link before starting.**

## Introduction to Genomics and Galaxy

This practical aims to familiarize you with the Galaxy user interface. It will teach you how to perform basic tasks such as importing data, running tools, working with histories, creating workflows, and sharing your work.

**For this workshop of November 2019, ignore the "Log in to Galaxy" section. We are going to use the EMBL instance. Also ignore the last part about repeating the analysis with a workflow.**

**Tutorial**: [Introduction to Genomics and Galaxy](https://galaxyproject.github.io/training-material/topics/introduction/tutorials/galaxy-intro-strands/tutorial.html).

**Differences with the tutorial:**
  * In UCSC, select "table: Comprehensive (wgEncodeGencodeCompV24" instead of "table: known genes".
  * When editing the name of the dataset, the button is not "Save attributes" but "Save".
  * To split the sequences, search for the term "filter"  instead of "split".
  * To intersect the data, use "Intersect intervals" in the "Bedtools" section.


## What is transcriptomics?

**Slides**: [Introduction to transcriptomics](https://galaxyproject.github.io/training-material/topics/transcriptomics/slides/introduction.html#1)

## About Quality control

**Slides**: [Quality control](https://galaxyproject.github.io/training-material/topics/sequence-analysis/tutorials/quality-control/slides.html#p1)

In this first tutorial, you will learn how to perform quality controls of your raw sequenced RNA-Seq data. It is necessary to understand, identify and exclude error-types that may impact the interpretation of downstream analysis. Sequence quality control is therefore an essential first step in your analysis. Catching errors early saves time later on.

**Tutorial**: [Quality Control](https://galaxyproject.github.io/training-material/topics/sequence-analysis/tutorials/quality-control/tutorial.html)

**Differences with the tutorial:**

Instead of using cutadapt we are going to use trim-galore:

  * Keep the first three 'single_end', 'reads_1', and 'automatic detection' options (should be the default).
  * Trim Galore! advanced settings: Full parameter list (leave the values to default, the important values to have in mind are mentioned here after).
    - Trim low-quality ends from reads in addition to adapter removal (Enter phred quality score threshold): 20
    - Discard reads that became shorter than length N: 20
  * Generate a report file: Yes
  * From the questions 'Inspect the generated txt file (Report)', you should find:
    - 26,809 reads with adapters.
    - 44,164 bp (1.2%) trimmed.
    - 347 (0.3%) sequences.

For the second trimming, select 'paired-end' and keep the other options as described above.
  * From the questions 'Inspect the generated txt file (Report)', you should find:
    - 44,164 bp (1.2%) for reads_1 and 138,638 bp (3.7%) for reads_2
    - 1,497 sequences have been removed because at least one read was shorter than the length cutoff (347 when only the forward reads were analyzed).
  
## About Mapping

**Slides**: [Mapping](https://galaxyproject.github.io/training-material/topics/sequence-analysis/tutorials/mapping/slides.html#p1)

In the following, we will process a dataset with the mapper Bowtie2 and we will visualize the data with the program IGV:

**Tutorial**: [Mapping](https://galaxyproject.github.io/training-material/topics/sequence-analysis/tutorials/mapping/tutorial.html)


## Getting started with RNA-Seq data

In this tutorial, we illustrate the analysis of the gene expression data step by step using 7 of the original datasets:

4 untreated samples: GSM461176, GSM461177, GSM461178, GSM461182
3 treated samples (Pasilla gene depleted by RNAi): GSM461179, GSM461180, GSM461181

Each sample constitutes a separate biological replicate of the corresponding condition (treated or untreated). Moreover, two of the treated and two of the untreated samples are from a paired-end sequencing assay, while the remaining samples are from a single-end sequencing experiment.

**You do not need to upload the data, you can find them in the shared library.**

**Tutorial**: [Reference-based RNA-Seq data analysis](https://galaxyproject.github.io/training-material/topics/transcriptomics/tutorials/ref-based/tutorial.html)

**Differences with the tutorial:**

1) Instead of using cutadapt we are going to use trim-galore:

  * Set the first option to 'paired_end'
  * Be careful about selecting '_1' and '_2' in the right section, the order is important here!
  * Trim Galore! advanced settings: Full parameter list (leave the values to default, the important values to have in mind are mentioned here after).
    - Trim low-quality ends from reads in addition to adapter removal (Enter phred quality score threshold): 20
    - Discard reads that became shorter than length N: 20
  * Generate a report file: Yes

2) Use sort in the 'text manipulation section' and **not** the one in the 'Filter and sort section'.

3) Skip the 'Gene Body Coverage' section.

4) Concerning the 'Identification of the differentially expressed features':

  * To understand the difference between RPKM, FPKM and TPM, watch this video: [Statquest - RPKM, FPKM and TPM, Clearly Explained](https://www.youtube.com/watch?v=TTUrtCY2k-w&t=113s)
  * To understand in more details how PCA works, watch this video: [Statquest - Principal component analysis step by step](https://www.youtube.com/watch?v=FgakZw6K1QQ)

5) In the 'Hands-on: Annotation of the differentially expressed genes' section:

  * 'Annotate DESeq2/DEXSeq output tables' is called 'Annotate DE(X)Seq result' in our instance.

6) In the 'Hands-on: Prepare the datasets for goseq' section:

  * When using 'compute', use c7<0.05 instead of bool(c7<0.05)
  
  
## Visualization of RNA-Seq results with Volcano Plot

The input data are based on an analysis performed with limma-voom but the procedure is similar if you used DESeq2; You just have to specify the correct columns numbers.

**You do not need to upload the data, you can find them in the shared library.**

**Tutorial**: [Visualization of RNA-Seq results with Volcano Plot](https://galaxyproject.github.io/training-material/topics/transcriptomics/tutorials/rna-seq-viz-with-volcanoplot/tutorial.html)

## Gene Set Enrichment Analysis

Sometimes there is quite a long list of genes to interpret after a differential expression analysis, and it is usually infeasible to go through the list one gene at a time trying to understand it’s biological function. A common downstream procedure is gene set testing, which aims to understand which pathways/gene networks the differentially expressed genes are implicated in. There are many different gene set testing methods that can be applied and it can be useful to try several.

**You do not need to upload the data, you can find them in the shared library.**

**Tutorial**: [RNA-seq genes to pathways](https://galaxyproject.github.io/training-material/topics/transcriptomics/tutorials/rna-seq-genes-to-pathways/tutorial.html)

**Differences with the tutorial**:

  * The 'goseq' section is similar to what you did in the 'Referenced-based RNA-Seq data annalysis': When using 'compute', use c8<0.01 instead of bool(c8<0.01)
  * In 'Hands-on: Prepare the two inputs for GOSeq' -> 'Join two Datasets side by side on a specified field', set 'Keep the header lines': No
  